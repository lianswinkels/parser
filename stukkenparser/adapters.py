from functools import partial
from typing import Any, Callable, Dict, List, Optional, Tuple, Union

import regex as re
from lxml import etree
from rdflib import BNode, Graph, Literal, Namespace, URIRef

import sys
##verwijs directory naar waar jij de parser scripts hebt staan dit moet dus aangepast worden!
sys.path.insert(0, 'C:/Users/Administrator/stukkenparser-main/stukkenparser')

from onderdelen import (
    Alinea,
    Box,
    DefItem,
    DefLijst,
    Figuur,
    Kop,
    Lijst,
    LijstItem,
    Margetekst,
    Onderdeel,
    Opsomming,
    Rij,
    Stuk,
    Tabel,
    TermItem,
    Tussenkop,
)


class XMLAdapter:
    """This class converts the parse tree of a document to XML.

    It is possible to convert an entire document or individual elements.

    Attributes:
        tree: parse tree or onderdeel
        is_stuk: True if `self.tree` is a complete document
        callbacks: dictionary with attribute callbacks (see :ref:`serialisatie-ref`)
    """

    def __init__(
        self,
        stuk: Stuk,
        callbacks: Dict[str, Callable[[Onderdeel], Dict[str, str]]] = {},
        default_attributes: bool = True,
    ) -> None:
        """Initialize the XMLAdapter.

        Args:
            stuk: Stuk or Onderdeel to convert to XML.
            callbacks: dictionary with callbacks ({"name": func})
            default_attributes: if False, override the default attribute callback
        """
        self.is_stuk = isinstance(stuk, Stuk)
        self.tree = stuk
        self.callbacks = {"default_attributes": self._default_attrs}
        if default_attributes:
            self.callbacks.update(callbacks)
        else:
            self.callbacks = callbacks

    def build(self) -> etree._Element:
        """Construct the XML tree."""
        if self.is_stuk:
            self._xml = etree.Element("document")
            self._xml.append(self._head())
            self._xml.append(self._body())
        else:
            self._xml = self._to_xml(self.tree)
        return self._xml

    def _head(self) -> etree._Element:
        """Process the document root."""
        head = etree.Element("head")
        for k, v in self.tree.metadata.items():
            try:
                elt = etree.Element("meta", name=k, content=v)
            except TypeError as e:
                raise TypeError(f"{k}: {v}") from e
            head.append(elt)
        return head

    def _body(self) -> etree._Element:
        body = etree.Element("body")
        for child in self.tree.children:
            body.append(self._to_xml(child))
        return body

    def _to_xml(self, ond: Union[Kop, Onderdeel]) -> etree._Element:
        """Construct the XML tree using Depth First Traversal."""
        if isinstance(ond, Kop):
            try:
                return self._container(ond)
            except TypeError as e:
                raise TypeError(f"{ond}") from e
        else:
            return self._onderdeel(ond)

    def _container(self, ond: Kop) -> etree._Element:
        attrs = self._get_attributes(ond)
        attrs["type"] = ond.type
        head = etree.Element("head")
        head.text = ond.text
        if ond.notes:
            head = self._add_notes(head, ond.notes)
        return self._div(attrs, children=[head] + [self._to_xml(c) for c in ond])

    def _onderdeel(self, ond: Onderdeel) -> etree._Element:
        attrs = self._get_attributes(ond)

        if isinstance(ond, Tabel):
            attrs["type"] = ond.type
            elt = self._table(ond, attrs)

        elif isinstance(ond, DefLijst):
            elt = self._def_list(ond, attrs)

        elif isinstance(ond, Lijst):
            elt = self._list(ond, attrs)

        elif isinstance(ond, Box):
            attrs["type"] = "box"
            elt = self._div(attrs, children=[self._onderdeel(i) for i in ond])

        elif isinstance(ond, Opsomming):
            attrs["type"] = "opsomming"
            elt = self._div(attrs, children=[self._onderdeel(i) for i in ond])

        elif isinstance(ond, Figuur):
            capt = self._elt("caption", text=ond.caption)
            attrs["src"] = ond.src
            elt = self._elt("img", attrs, children=[capt])

        else:
            elt = self._elt("p", attrs, text=ond.text)
            if ond.notes:
                elt = self._add_notes(elt, ond.notes)

        return elt

    def _default_attrs(self, ond: Onderdeel) -> Dict[str, Any]:
        attrs = {
            attr: str(getattr(ond, attr, None))
            for attr in ["id", "index", "level"]
            if hasattr(ond, attr)
        }
        return attrs

    def _get_attributes(self, ond: Onderdeel) -> Dict[str, str]:
        """Apply callbacks to an Onderdeel to extract attributes."""
        attrs = {}
        for _, func in self.callbacks.items():
            res = func(ond)
            if res:
                if isinstance(res, dict):
                    attrs.update(res)
        return attrs

    def _list(
        self,
        ond: Lijst,
        attrs: Dict[str, str] = {},
    ) -> etree._Element:
        elt = etree.Element("list", **attrs)
        for item in ond:
            elt.append(self._item(item))
        return elt

    def _item(
        self,
        ond: LijstItem,
    ) -> etree._Element:
        attrs = self._get_attributes(ond)
        attrs["label"] = ond.label
        elt = self._elt("item", attrs)
        if ond.has_head:
            elt.append(self._elt("p", text=ond.text))
        elif ond.has_list:
            elt.append(self._list(ond.lijst))
        else:
            raise ValueError(
                f"Encountered empty LijstItem:\n\t{etree.tostring(ond.elt)}"
            )
        return elt

    def _def_list(
        self,
        ond: DefLijst,
        attrs: Dict[str, str] = {},
    ) -> etree._Element:
        elt = etree.Element("deflist", **attrs)
        for item in ond:
            elt.append(self._def_item(item))
        return elt

    def _def_item(
        self,
        ond: Union[TermItem, DefItem],
    ) -> etree._Element:
        attrs = self._get_attributes(ond)
        if isinstance(ond, TermItem):
            elt = self._elt("term", attrs, text=ond.text)
        elif isinstance(ond, DefItem):
            elt = self._elt("definition", attrs)
            if ond.has_head:
                elt.append(self._elt("p", text=ond.text))
            if ond.has_list:
                if isinstance(ond.lijst, DefLijst):
                    elt.append(self._def_list(ond.lijst))
                if isinstance(ond.lijst, Lijst):
                    elt.append(self._list(ond.lijst))
        return elt

    def _add_notes(self, elt: etree._Element, notes: Dict[str, str]) -> etree._Element:
        for k, v in notes.items():
            try:
                n_re = re.compile(rf"\[{k}\]")
                if n_re.search(elt.text):
                    txt, tail = n_re.split(elt.text, maxsplit=1)
                else:
                    continue
            except ValueError as e:
                raise ValueError(f"{notes}, {k}, {elt.text}") from e
            except AttributeError as a:
                raise AttributeError(f"{elt}") from a
            elt.text = txt
            note = self._elt("note", {"num": k}, text=v)
            note.tail = tail
            elt.append(note)
        return elt

    def _table(self, ond: Tabel, attrs: Dict[str, str] = {}) -> etree._Element:
        capt = self._elt("caption", text=ond.caption)
        attrs["thousep"] = ond.thousep
        attrs["decsep"] = ond.decsep
        attrs["NaN"] = ond.NaN
        if ond.notes:
            capt = self._add_notes(capt, ond.notes)
        rows = [self._row(r) for r in ond.rows]
        return self._elt("table", attrs, children=[capt] + rows)

    def _row(self, ond: Rij) -> etree._Element:
        row = []
        for cell in ond.cells:
            elt = self._elt("cell", text=cell.text, attrs={"dtype": cell.dtype})
            if ond.notes:
                self._add_notes(elt, ond.notes)
            row.append(elt)

        attrs = {"id": ond.id}
        if ond.header:
            attrs["type"] = "header"
        return self._elt("row", attrs, children=row)

    def _div(
        self,
        attrs: Dict[str, str] = {},
        children: List[etree._Element] = [],
    ) -> etree._Element:
        try:
            elt = etree.Element("div", **attrs)
        except TypeError as e:
            raise TypeError(f"{attrs}") from e
        for child in children:
            elt.append(child)
        return elt

    def _elt(
        self,
        type: str,
        attrs: Dict[str, str] = {},
        text: Optional[str] = None,
        children: List[etree._Element] = [],
    ) -> etree._Element:
        """Helper method that constructs an XML element."""
        try:
            elt = etree.Element(type, **attrs)
        except TypeError as e:
            raise TypeError(f"{type}, {attrs}") from e
        if text is not None:
            elt.text = text
        for ch in children:
            try:
                elt.append(ch)
            except TypeError as e:
                raise TypeError(f"{children}") from e
        return elt

    def to_string(self) -> str:
        """Return an indented and pretty_printed string."""
        if not hasattr(self, "_xml"):
            self.build()
        etree.indent(self._xml)
        return etree.tostring(self._xml, encoding=str, pretty_print=True)


# Type aliases
Term = Union[URIRef, Literal, BNode]
Triple = Tuple[Term, Term, Term]


class RDFAdapter:
    """The RDFAdapter class converts the parse tree of a document to a linked data graph.

    Attributes:
        graph: Linked data graph
        metadata: metadata associated with the document
        callbacks: dictionary with attribute callbacks (see :ref:`serialisatie-ref`)
        ns: namespaces associated with the document components
        dispatch: dictionary that maps onderdelen to specific processing methods
    """

    def __init__(
        self,
        stuk: Stuk,
        callbacks: Dict[str, Callable[[Onderdeel], Dict[str, str]]] = {},
        namespaces: Dict[str, str] = {
            "rdf": "http://placeholder/rdf/",
            "ftext": "https://www.rijksfinancien.nl/ftext/",
            "doco": "http://purl.org/spar/doco/",
            "po": "http://purl.org/spar/po/",
            "dct": "http://purl.org/dc/terms/",
            "schema": "http://schema.org/",
        },
    ) -> None:
        """Initialize the adapter.

        Arguments:
            stuk: parse tree
            callbacks: Optional dictionary with attribute callbacks (see :ref:`serialisatie-ref`)
            namespaces: <prefix> : <namespace> mapping
        """
        self.stuk = stuk
        self.metadata = stuk.metadata
        self.callbacks = callbacks

        self.ns = {}
        self.graph = Graph()
        for pref, ns in namespaces.items():
            self.graph.bind(prefix=pref, namespace=Namespace(ns))
            self.ns[pref] = Namespace(ns)

        self.dispatch = {
            Kop: self._section,
            Tussenkop: self._section,
            Margetekst: self._section,
            Stuk: self._root,
            Tabel: self._tabel,
            Rij: self._rij,
            Lijst: self._lijst,
            DefLijst: self._def_lijst,
            LijstItem: self._lijst_item,
            TermItem: self._def_item,
            DefItem: self._def_item,
            Opsomming: self._opsomming,
            Box: self._box,
            Figuur: self._figuur,
            Alinea: self._alinea,
        }

    def build(self) -> None:
        """Build the knowledge graph."""
        for ond in self.stuk.onderdelen():
            self._to_rdf(ond)

    @staticmethod
    def triple(s: Term, p: Term, o: Term) -> Triple:
        """Create a tuple of three RDF terms."""
        return (s, p, o)

    def term(self, name: str, label: str) -> Term:
        """Create an RDF term of the form <namespace>:<label>."""
        return self.ns[name][label]

    def _graph_add(self, o: Term, v: Term) -> None:
        self.graph.add(self.current_subject(o, v))

    def _to_rdf(self, ond: Onderdeel) -> None:
        # Create a closure that returns a function -> (Current Subject, Term, Term)
        self.current_subject = partial(self.triple, self.term("ftext", ond.id))

        self._graph_add(self.term("dct", "identifier"), Literal(ond.id))
        self._graph_add(self.term("rdf", "type"), self.term("ftext", ond.type))

        if ond.parent:
            self._graph_add(
                self.term("po", "isContainedBy"), self.term("ftext", ond.parent.id)
            )

        self.dispatch[type(ond)](ond)

        self.apply_callbacks(ond)

    def apply_callbacks(self, ond: Onderdeel) -> None:
        """Apply callbacks. """
        for _, func in self.callbacks.items():
            res = func(ond)
            if res:
                for p, o in res:
                    self._graph_add(self.term("ftext", p), Literal(o))

    def _add_children(self, ond: Onderdeel) -> None:
        for child in ond:
            self._graph_add(self.term("po", "contains"), self.term("ftext", child.id))

    def _root(self, ond: Onderdeel) -> None:
        self._graph_add(self.term("rdf", "type"), self.term("dct", ond.phase))
        for k, v in self.metadata.items():
            if k.startswith("DC"):
                _, term = k.split(".")
                self._graph_add(self.term("dct", term), Literal(v))
        self._add_children(ond)

    def _section(self, ond: Kop) -> None:
        self._graph_add(self.term("rdf", "type"), self.term("doco", "Section"))
        self._graph_add(self.term("schema", "text"), Literal(ond.text))
        self._add_children(ond)

    def _tabel(self, ond: Tabel) -> None:
        self._graph_add(self.term("rdf", "type"), self.term("doco", "Table"))
        self._graph_add(self.term("schema", "text"), Literal(ond.caption))
        self._add_children(ond)

    def _rij(self, ond: Rij) -> None:
        self._graph_add(self.term("rdf", "type"), self.term("po", "Record"))
        for cell in ond:
            cell_s = BNode()
            self._graph_add(self.term("po", "contains"), cell_s)
            self.graph.add((cell_s, self.term("rdf", "type"), self.term("po", "Field")))
            self.graph.add((cell_s, self.term("schema", "text"), Literal(cell.text)))

    def _lijst(self, ond: Lijst) -> None:
        self._graph_add(self.term("rdf", "type"), self.term("doco", "List"))
        self._add_children(ond)

    def _lijst_item(self, ond: LijstItem) -> None:
        self._graph_add(self.term("rdf", "type"), self.term("rdf", "li"))
        self._graph_add(self.term("ftext", "Label"), Literal(ond.label))
        if ond.has_head:
            self._graph_add(self.term("schema", "text"), Literal(ond.text))
        if ond.has_list:
            self._graph_add(
                self.term("po", "contains"), self.term("ftext", ond.lijst.id)
            )
            self._lijst(ond.lijst)

    def _def_lijst(self, ond: DefLijst) -> None:
        self._graph_add(self.term("rdf", "type"), self.term("doco", "List"))
        self._add_children(ond)

    def _def_item(self, ond: Union[TermItem, DefItem]) -> None:
        self._graph_add(self.term("rdf", "type"), self.term("rdf", "li"))
        if isinstance(ond, TermItem):
            self._graph_add(self.term("rdf", "type"), self.term("ftext", "term"))
            self._graph_add(self.term("schema", "text"), Literal(ond.text))
        elif isinstance(ond, DefItem):
            self._graph_add(self.term("rdf", "type"), self.term("ftext", "definitie"))
            if ond.has_head:
                self._graph_add(self.term("schema", "text"), Literal(ond.text))
            if ond.has_list:
                self._graph_add(
                    self.term("po", "contains"), self.term("ftext", ond.lijst.id)
                )
                self._lijst(ond.lijst)

    def _box(self, ond: Box) -> None:
        self._graph_add(self.term("rdf", "type"), self.term("doco", "TextBox"))
        self._add_children(ond)

    def _opsomming(self, ond: Opsomming) -> None:
        self._graph_add(self.term("rdf", "type"), self.term("po", "Pattern"))
        self._add_children(ond)

    def _figuur(self, ond: Figuur) -> None:
        self._graph_add(self.term("rdf", "type"), self.term("doco", "Figure"))
        self._graph_add(self.term("schema", "text"), Literal(ond.caption))
        self._graph_add(self.term("dct", "source"), Literal(ond.src))

    def _alinea(self, ond: Alinea) -> None:
        self._graph_add(self.term("rdf", "type"), self.term("doco", "Paragraph"))
        self._graph_add(self.term("schema", "text"), Literal(ond.text))

    def to_string(self, fmt: str = "turtle") -> str:
        """Returns a string representation of `self.graph`.

        Attributes:
            fmt: RDF serialization language. Possible values are:
                 "n3", "nquads", "nt", "rdfxml", "trig", "trix", "turtle".
                 Default: turtle
        """
        if not self.graph:
            self.build()
        return self.graph.serialize(format=fmt).decode("utf-8")
