#! /usr/bin/env python

from typing import Any, Callable, Dict, Iterator, List, Optional, Pattern, Union

import lxml.html
import more_itertools as mi
import regex as re
from lxml import etree

import sys
##verwijs directory naar waar jij de parser scripts hebt staan dit moet dus aangepast worden!
sys.path.insert(0, 'C:/Users/Administrator/stukkenparser-main/stukkenparser')

from text_utils import TextNormaliser, removeprefix

# type aliases
Child = Union["Onderdeel", "LijstItem", "Rij"]

OND_TYPES: Dict[str, str] = {
    "Alinea": "alinea",
    "Box": "box",
    "Cel": "cel",
    "DefItem": "definitie",
    "DefLijst": "definitielijst",
    "Figuur": "figuur",
    "Kop": "kop",
    "Lijst": "lijst",
    "LijstItem": "item",
    "Margetekst": "margetekst",
    "Opsomming": "opsomming",
    "Rij": "rij",
    "Stuk": "stuk",
    "Tabel": "tabel",
    "TermItem": "term",
    "Tussenkop": "tussenkop",
}


class Onderdeel:
    """Onderdeel Represents a token found in a document.

    Each token type is a subclass of this class.

    Attributes:
        elt: html element that contains this Onderdeel's data
        parent: this object's parent
        config: configuration specific to this Onderdeel
        type_map: class variable mapping class name to Onderdeel type
    """

    def __init__(self, elt: lxml.html.HtmlElement, config: Dict[str, Any] = {}) -> None:
        self.elt = elt
        self.parent: Optional[Any] = None
        self.config = Config(self.token.lower(), config)
        self.normaliser = TextNormaliser()
        self.type_map: Dict[str, str] = (
            OND_TYPES if not hasattr(self.config, "type_map") else self.config.type_map
        )

    @property
    def root(self) -> "Onderdeel":
        """Return the root of the document."""
        if not hasattr(self, "_root"):
            if self.parent is not None:
                self._root = self.parent.root
            else:
                self._root = self
        return self._root

    @property
    def dossier(self) -> str:
        """Dossier number, e.g. *kst-34567-A-1*."""
        return self.root.dossier

    @property
    def chapter(self) -> str:
        """Department the budget belongs to."""
        return self.root.chapter

    @property
    def number(self) -> str:
        """OVERHEIDop.ondernummer."""
        return self.root.number

    @property
    def year(self) -> str:
        """Budget year."""
        return self.root.year

    @property
    def phase(self) -> str:
        """Budget phase, e.g. *JV* for *jaarverslag*."""
        return self.root.phase

    @property
    def token(self) -> str:
        """Lower cased class name.

        Use to check token type without importing every subclass.
        """
        return self.__class__.__name__

    @property
    def text(self) -> str:
        """Normalised text content. Normalisation consists of:

        * replacing repeated whitespace (including non-breaking spaces) with a single whitespace
        * removing arbitrary linebreaks
        * replacing various types of dashes (m-, n-, box lines) with minus ("-")
        * replacing double and single quotation marks, including guillements ('«', '»'), with apostrophes ("'")
        * extracting and resolving footnotes
        * normalise unicode (NFC)
        """
        if not hasattr(self, "_text"):
            self._text = self._get_text(self.elt)
        return self._text

    @property
    def notes(self) -> Dict[str, str]:
        """Dict containing footnotes found in `self.text`."""
        if not hasattr(self, "_notes"):
            self._notes = {}
            for link in self.elt.xpath(".//a[contains(@class, 'nootnum')]"):
                for foot in self.elt.xpath("//div[contains(@class, 'noot')]"):
                    if "href" in link.attrib and "id" in foot.attrib:
                        if link.get("href").strip("#") in foot.get("id", ""):
                            self._notes[link.text.strip("[]")] = self._get_text(
                                foot.find("./p")
                            )
        return self._notes

    @property
    def index(self) -> Optional[str]:
        """position in `self.parent.children`, or 1 if Root."""
        if self.parent is not None:
            return f"{self.parent.children.index(self)+1}"
        return "1"

    @property
    def id(self) -> Optional[str]:
        """Unique index, breadcrumb trail of chained id's."""
        if self.parent is not None:
            return f"{self.parent.id}.{self.parent.children.index(self)+1}"
        return "1"

    @property
    def type(self) -> str:
        """Determines the onderdeel's type. Default types are listed in `self.type_map`.

        If one of the regexes specified in `self.config.types` matches, the Onderdeel
        will be assigned its accompanying type. See also: :ref:`config-ref`

        NB: this method uses `re.search <https://docs.python.org/3/library/re.html#re.search>_`.
        """
        if not hasattr(self, "_type"):
            self._type = self.type_map[self.token]
            if hasattr(self.config, "types"):
                for k, v in self.config.types.items():
                    if re.search(k, self.text):
                        self._type = v
        return self._type

    def _get_text(self, elt: lxml.html.HtmlElement) -> str:
        notes = self.elt.xpath(".//a[contains(@class, 'nootnum')]")
        for note in notes:
            if "[" in note.text:
                continue
            note.text = f"[{note.text}]"

        content = []
        for t in elt.text_content().split("\n"):
            content.append(self.normaliser.normalise_all(t).strip())
        return " ".join(content).strip()

    def to_dict(self) -> Dict[str, Any]:
        return {
            "id": self.id,
            "index": self.index,
            "token": self.token,
            "type": self.type,
            "parent": self.parent.id,
            "text": self.text,
            "notes": self.notes,
        }

    def __repr__(self) -> str:
        return f"<{self.token} {self.type} {self.elt.tag} {self.elt.attrib}>"


class Config:
    """The Config class holds configuration data specific to each Onderdeel.

    Attributes:
        ond: token of the Onderdeel that contains this
        embedding: dict with configuration data for embedding levels (see :ref:`config-ref`)
        types: dict with configuration data for the object's type property (see :ref:`config-ref`)
    """

    embedding: Dict[str, Any]
    types: Dict[str, Any]

    def __init__(self, ond: str, config: Dict[str, Any]) -> None:
        """Create the config with data specific to it's Onderdeel."""
        self.ond = ond
        self.fields = set()
        for k, v in config.items():
            if isinstance(v, dict) and self.ond in v:
                self.fields.add(k)
                if hasattr(self, k):
                    self.__dict__[k].update(v[self.ond])
                else:
                    setattr(self, k, v[self.ond])
            else:
                setattr(self, k, v)

    @classmethod
    def from_ond_dict(cls, ond: Dict[str, Any]) -> "Config":
        """Initialize Config from a non-specific dict."""
        conf = cls.__new__(cls)

        data = ond["config"]

        conf.ond = ond["type"]
        setattr(conf, "fields", set(data["fields"]))
        for field in conf.fields:
            setattr(conf, field, data[field])

        return conf

    def to_dict(self) -> Dict[str, Any]:
        """Convert Config to serializable dict."""
        data = {"fields": list(self.fields)}
        for f in self.fields:
            data[f] = getattr(self, f)
        return data


class Container:
    """Mixin for container tokens. Implements iterable pattern."""

    children: List[Child]
    _items: Callable[[Onderdeel], List[Child]]

    def __init__(self, *args: Any) -> None:
        super().__init__(*args)
        self.children = []
        if hasattr(self, "_items"):
            self._items()

    def add(self, ond: Onderdeel) -> None:
        """Add `ond` as a child."""
        ond.parent = self
        self.children.append(ond)

    def to_dict(self) -> Dict[str, Any]:
        return {
            "id": self.id,
            "index": self.index,
            "token": self.token,
            "type": self.type,
            "parent": self.parent.id,
            "children": [child.to_dict() for child in self],
        }

    def __len__(self) -> int:
        return len(self.children)

    def __iter__(self) -> Iterator[Onderdeel]:
        return iter(self.children)


class Kop(Container, Onderdeel):
    """Top-level Container class.

    The Kop class represents <h\ *n*\> elements. Its children consist of the parts of the document
    that belong to document section it is associated with.

    `Kop` is the parent of a section. The Container mixin provides `add()` and
    implements the iterable pattern.

    Attributes:
        children: the sections content.
    """

    _level: Union[int, float]

    @property
    def level(self) -> Union[int, float]:
        """level is used during parsing to determine the level of embedding.

        The level consists of the numerical part of the h-tag, but it can also be configured (see :ref:`config-ref`).
        """
        if not hasattr(self, "_level"):
            self._level = int(list(self.elt.tag)[-1])

            if hasattr(self.config, "embedding"):
                for k, v in self.config.embedding.get("titel", {}).items():
                    if re.match(k, self.text):
                        self._level = v

        return self._level

    @level.setter
    def level(self, lvl: Union[int, float]) -> None:
        self._level = lvl

    def to_dict(self) -> Dict[str, Any]:
        return {
            "id": self.id,
            "index": self.index,
            "parent": self.parent.id,
            "token": self.token,
            "type": self.type,
            "level": self.level,
            "text": self.text,
            "notes": self.notes,
            "children": [child.to_dict() for child in self],
        }

    def __repr__(self) -> str:
        return f'<{self.token} class={self.elt.get("class")} type={self.type} - "{self.text.strip()}">'


class Tussenkop(Kop):
    """Mid-level Container class.

    The Tussenkop class represents `<p class="tussenkop">` elements. Its children
    consist of the parts of the document that belong to document section it is the header of.
    A `Tussenkop` can be a child of a `Kop`, but not vice versa.

    `Tussenkop` is the parent of a section. The Container mixin provides `add()` and
    implements the iterable pattern.

    Attributes:
        children: the section's content
    """

    idx_re: Pattern = re.compile(
        r"(?:^[A-Za-z]+\s)?(([A-Z]([0-9]+)?[:.) ])|\d+\.?(?:\d\.?)*)"
    )

    @property
    def opmaak(self) -> str:
        if not hasattr(self, "_opmaak"):
            self._opmaak = ""
            if self.elt.find("strong") is not None:
                p_class = self.elt.find("strong")
                self._opmaak = p_class.get("class")
        return self._opmaak

    @property
    def level(self) -> Union[int, float]:
        """Level is used during parsing to determine the level of embedding.

        The level of a `Tussenkop` is determined using `self.idx_re`. If the text matches, the
        content of the match is split to determine the amount of levels the match contains. E.g.
        the match "1.02.03" is three levels deep.

        Additionally, levels may be configured (see :ref:`config-ref`).
        """
        _idx = self.idx_re.match(self.text)

        if _idx is not None:
            self._level = len(_idx.group().strip(".:").split("."))
        elif hasattr(self.config, "embedding") and "opmaak" in self.config.embedding:
            self._level = float(self.config.embedding["opmaak"].get(self.opmaak, 8))

        elif hasattr(self.config, "embedding") and "titel" in self.config.embedding:
            for k, v in self.config.embedding.get("titel", {}).items():
                if re.match(k, self.text):
                    self._level = v
                else:
                    self._level = 8
        else:
            # Manually set levels have priority if the regex doesnt match
            if not hasattr(self, "_level"):
                self._level = 8
        return self._level

    @level.setter
    def level(self, lvl: Union[int, float]) -> None:
        self._level = lvl

    def to_dict(self) -> Dict[str, Any]:
        return {
            "id": self.id,
            "index": self.index,
            "parent": self.parent.id,
            "token": self.token,
            "type": self.type,
            "level": self.level,
            "opmaak": self.opmaak,
            "text": self.text,
            "notes": self.notes,
            "children": [child.to_dict() for child in self],
        }

    def __repr__(self) -> str:
        return f'<{self.token}, level={self.level}, opmaak={self.opmaak}, type={self.type} - "{self.text.strip()}">'


class Margetekst(Tussenkop):
    """Bottom-level Container class.

    The Margetekst class represents `<div class="margetekst">` elements. Its children
    consist of the parts of the document that belong to document section it is the header of.
    An instance of `Margetekst` can be a child of instances of `Kop` or `Tussenkop`, but not vice versa.

    `Margetekst` is the parent of a section. The `Container` mixin provides `add()` and
    implements the iterable pattern.

    Attributes:
        children: the section's content
    """

    @property
    def level(self) -> int:
        """Level is used during parsing to determine the level of embedding. Its value is 9 (hardcoded)"""
        return 9

    @level.setter
    def level(self, lvl: Union[int, float]) -> None:
        self._level = lvl


class Alinea(Onderdeel):
    """The Alinea token represents standard paragraphs, i.e. all `<p>` elements without a class."""

    pass


class Figuur(Onderdeel):
    """Figuur represents `<div class="plaatje">` elements."""

    @property
    def text(self) -> str:
        return self.caption

    @property
    def attributes(self) -> Dict[str, str]:
        """Contains `<img>` element attributes"""
        return self.elt.find("img").attrib

    @property
    def alt(self) -> str:
        """The image's alt text. Returns None if there is no alt property."""
        return self.attributes.get("alt", "")

    @property
    def src(self) -> str:
        """Source filename."""
        if not hasattr(self, "_src"):
            self._src = f"{self.attributes.get('src')}"
        return self._src

    @property
    def caption(self) -> str:
        """Image caption. Also returned by `self.text`"""
        if not hasattr(self, "_caption"):
            _capt = self.elt.find("./p")
            self._caption = self._get_text(_capt) if _capt is not None else ""
        return self._caption

    def to_dict(self) -> Dict[str, Any]:
        return {
            "id": self.id,
            "index": self.index,
            "parent": self.parent.id,
            "token": self.token,
            "type": self.type,
            "text": self.text,
            "alt": self.alt,
            "notes": self.notes,
            "src": self.src,
        }


class LijstItem(Onderdeel):
    """Representation of `<li>` elements.

    Attributes:
        has_list: True if this element contains a list
        has_head: True if this element contains text
        lijst: an instance of Lijst if `has_list` is True
    """

    def __init__(self, elt: lxml.html.HtmlElement, config: Dict[str, Any] = {}) -> None:
        super().__init__(elt, config)
        self.has_list = True if self.elt.find("ul") is not None else False
        self.has_head = True if self.elt.find("p") is not None else False
        if self.has_list:
            self.lijst = Lijst(self.elt.find("ul"))
            self.lijst.parent = self

    @property
    def label(self) -> str:
        """Symbol that precedes a list item."""
        if not hasattr(self, "_label"):
            span = self.elt.find("./p/span")
            self._label = self._get_text(span) if span is not None else ""
        return self._label

    @property
    def text(self) -> str:
        if not hasattr(self, "_text"):
            self._text = removeprefix(
                self._get_text(self.elt.find("./p")), f"{self.label} "
            )
        return self._text

    def to_dict(self) -> Dict[str, Any]:
        d = {
            "id": self.id,
            "index": self.index,
            "parent": self.parent.id,
            "token": self.token,
            "type": self.type,
            "label": self.label,
            "text": self.text,
            "notes": self.notes,
        }
        if self.has_head:
            d["text"] = self.text
        if self.has_list:
            d["children"] = [self.lijst]
        return d

    def __iter__(self) -> Iterator[Onderdeel]:
        if self.has_list:
            return iter(self.lijst)
        else:
            return iter([])

    def __len__(self) -> int:
        if self.has_list:
            return len(self.lijst)
        else:
            return 0


class DefItem(LijstItem):
    """<dd>"""

    @property
    def label(self) -> None:
        "Overridden"
        return None


class TermItem(Alinea):
    """<dt>"""

    @property
    def text(self) -> str:
        if not hasattr(self, "_text"):
            self._text = self._get_text(self.elt).strip(":")
        return self._text


class Lijst(Container, Onderdeel):
    """Token for `<ul>` elements.

    An instance of `Lijst` contains instances of `LijstItem`. `Container` provides
    `add()` and implements the iterable pattern.

    Attributes:
        children: a list of `LijstItem`.
    """

    @property
    def text(self) -> str:
        # Overridden: text is contained in list items.
        return ""

    def _items(self) -> List[Child]:
        """Parses the internal structure of the <ul> element."""
        for item in self.elt.xpath("./li"):
            self.add(LijstItem(item))
        return self.children

    @property
    def id(self) -> Optional[str]:
        """Consists of an Onderdeel's index chained to the index of its parent."""
        if not hasattr(self, "_id"):
            if self.parent is not None:
                if isinstance(self.parent, LijstItem):
                    self._id = f"{self.parent.id}"
                else:
                    self._id = f"{self.parent.id}.{self.parent.children.index(self)+1}"
            else:
                self._id = "1"
        return self._id


class DefLijst(Lijst):
    """Token for `<dl>` elements.

    An instance of `DefLijst` contains instances of `TermItem` and `DefItem`. `Container` provides
    `add()` and implements the iterable pattern.

    Attributes:
        children: a list of `DefLijstItem`.
    """

    def _items(self) -> List[Child]:
        """Parses the internal structure of the <ul> element."""
        for item in self.elt.xpath("./dt|./dd"):
            if item.tag == "dt":
                self.add(TermItem(item))
            elif item.tag == "dd":
                self.add(DefItem(item))
        return self.children


class Opsomming(Container, Onderdeel):
    """Represents `<div class="alineagroep">` elements.

    An instance of `Opsomming` contains one or more instances of either `Alinea`, `Lijst` or a
    combination of those. `Container` provides `add()` and implements the iterable pattern.

    Attributes:
        children: a list of `Alinea` and/or `Lijst`.
    """

    @property
    def text(self) -> str:
        return ""

    def _items(self) -> List[Child]:
        """Parses the internal structure of the div."""
        for elt in self.elt:
            if elt.tag == "p":
                self.add(Alinea(elt))
            elif elt.tag == "ul":
                self.add(Lijst(elt))
            elif elt.tag == "table":
                self.add(Tabel(elt, self.config.to_dict()))
            else:
                raise ValueError(
                    f"{self.token} ({lxml.html.tostring(self.elt)}) encountered unexpected element: {elt.tag} {elt.attrib}"
                )
        return self.children


class Box(Opsomming):
    """<div class="box"."""

    def _items(self) -> List[Child]:
        """Parses the internal structure of the div."""
        for elt in self.elt:
            if elt.tag == "p":
                self.add(Alinea(elt))
            elif elt.tag == "ul":
                self.add(Lijst(elt))
            elif elt.tag == "div":
                elt_cls = elt.get("class", "")
                if "alineagroep" in elt_cls:
                    self.add(Opsomming(elt, self.config.to_dict()))
                elif "tabelnoten" in elt_cls:
                    continue
            elif elt.tag == "table":
                self.add(Tabel(elt, self.config.to_dict()))
            else:
                raise ValueError(
                    f"{self.token} encountered unexpected element: {elt.tag} {elt.attrib}"
                )
        return self.children


class Cel(Onderdeel):
    """Represents <td> elements.

    Attributes:
        row: the `Rij` this element belongs to
        NaN: representation of NaN values
        dtype: the cell's data type
    """

    def __init__(self, elt: lxml.html.HtmlElement, row: "Rij") -> None:
        super().__init__(elt)
        self.row: Rij = row
        self.NaN: str = self.row.parent.NaN
        self.dtype: str = (
            "NaN"
            if self.data(self.elt) == self.NaN
            else self.data(self.elt).__class__.__name__
        )

    @property
    def text(self) -> Union[str, float]:
        if not hasattr(self, "_text"):
            self._text = self.data(self.elt)
        return f"{self._text}"

    def data(self, elt: lxml.html.HtmlElement) -> Union[str, float]:
        """Normalise the cell's text.

        Try to convert the cell's data to float if it looks like a number.
        If not, or if conversion fails, return a string.
        """
        # sometimes people like to use <td> as rows and <p> as cells...
        if len(elt) >= 2:
            data = " ".join(self._get_text(p) for p in elt.xpath("./p"))
        else:
            data = self._get_text(elt)

        if not data:
            return self.NaN
        elif self.row.header or re.search(r"[a-zA-Z()><€/%]+", data, re.U):
            return data
        else:
            # It's hard to differentiate between "-" (empty cell),
            # "201x-201x" and negative numbers, so just catch the error here
            try:
                return self._to_float(data)
            except ValueError:
                return data

    def _to_float(self, data: str) -> float:
        data = (
            data.replace(" ", "")
            .replace(self.row.parent.thousep, "")
            .replace(self.row.parent.decsep, ".")
        )
        return float(data)

    def to_dict(self) -> Dict[str, Any]:
        return {
            "id": self.id,  # this could error
            "index": self.index,
            "parent": self.row.id,
            "token": self.token,
            "type": self.type,
            "dtype": self.dtype,
            "text": self.text,
            "notes": self.notes,
        }


class Rij(Onderdeel):
    """Iterator, represents table rows (<th>, <td>).

    Attributes:
        header: indicates if an instance of `Rij` is a header row
    """

    def __init__(self, elt: lxml.html.HtmlElement, header: bool = False) -> None:
        super().__init__(elt)
        self.header = header

    @property
    def text(self) -> str:
        return ""

    @property
    def cells(self) -> List[Cel]:
        if not hasattr(self, "_cells"):
            self._cells = [Cel(c, self) for c in self.elt.xpath(".//th|.//td")]
        return self._cells

    def __iter__(self) -> Iterator[Cel]:
        return iter(self.cells)

    def __getitem__(self, idx: int) -> Cel:
        return self.cells[idx]

    def __len__(self) -> int:
        return len(self.cells)

    def to_dict(self) -> Dict[str, Any]:
        return {
            "id": self.id,
            "index": self.index,
            "parent": self.parent.id,
            "token": self.token,
            "type": self.type,
            "header": self.header,
            "cells": [cell.to_dict() for cell in self],
        }


class Tabel(Container, Onderdeel):
    """Represents tables.

    An instance of `Tabel` contains instances of `Rij`. `Container` provides
    `add()`.

    Attributes:
        thousep: thousands separator. Default '.'
        decsep: decimal separator. Default ','
        NaN: empty cells etc. Default 'NaN'
    """

    def __init__(self, elt: lxml.html.HtmlElement, config: Dict[str, Any] = {}) -> None:
        super().__init__(elt, config)
        self.thousep: str = "."
        self.decsep: str = ","
        self.NaN: str = "NaN"

    @property
    def text(self) -> str:
        """Table caption plus table content."""
        if not hasattr(self, "_text"):
            self._text = self.caption
        return self._text

    @property
    def caption(self) -> str:
        """Table caption."""
        if not hasattr(self, "_caption"):
            _capt = self.elt.find("./caption")
            self._caption = self._get_text(_capt) if _capt is not None else ""
        return self._caption

    @property
    def rows(self) -> List[Rij]:
        if not hasattr(self, "_rows"):
            self._rows = self._items()
            if not self._rows[0].header:
                self._rows[0].header = True
        return self._rows

    def _items(self) -> List[Rij]:
        """Return a list of table rows"""
        for row in self.elt.xpath(".//thead/tr"):
            self.add(Rij(row, header=True))
        for row in self.elt.xpath(".//tbody/tr"):
            self.add(Rij(row))
        return self.children

    def __iter__(self) -> Iterator[Rij]:
        """Overrides `Container.__iter__()`."""
        return iter(self.rows)

    def __getitem__(self, idx: int) -> Rij:
        return self.rows[idx]

    def __len__(self) -> int:
        """Overrides `Container.__len__()`."""
        return len(self.rows)

    def to_dict(self) -> Dict[str, Any]:
        return {
            "id": self.id,
            "index": self.index,
            "parent": self.parent.id,
            "token": self.token,
            "type": self.type,
            "thousep": self.thousep,
            "decsep": self.decsep,
            "NaN": self.NaN,
            "caption": self.text,
            "notes": self.notes,
            "rows": [row.to_dict() for row in self],
        }


class Stuk(Container, Onderdeel):
    """Document root.

    All elements of a document are descendants of the Root element. Additionally, the root
    contains a document's metadata and config.

    Attributes:
        config: the original configuration dict, rather than a config object.
                mainly for logging purposes
    """

    def __init__(self, elt: lxml.html.HtmlElement, config: Dict[str, Any] = {}) -> None:
        super().__init__(elt, {})
        self.config: Dict[str, Any] = config
        self.metadata_keys = ["dc", "dcterms", "dct", "overheid", "overheidop"]

    @property
    def metadata(self) -> Dict[str, str]:
        """DCTERMS and OVERHEIDop metadata found in the html."""
        if not hasattr(self, "_metadata"):
            self._metadata = {}
            for e in self.elt.xpath(".//meta"):
                key = e.get("name", "")
                if re.match("|".join(self.metadata_keys), key, re.I):
                    self._metadata[key] = e.get("content")
            try:
                self._metadata[
                    "source"
                ] = f"https://zoek.officielebekendmakingen.nl/{self._metadata['DC.identifier']}.html"
            except KeyError:
                pass
                # raise KeyError(f"metadata: {self._metadata}") from e
                # print(f"Key {e.args[0]} not found.")

        return self._metadata

    @property
    def dossier(self) -> str:
        if not hasattr(self, "_dossier"):
            m = re.search(r"\d{5}", self.metadata.get("OVERHEIDop.dossiernummer", ""))
            self._dossier = m.group() if m else ""
        return self._dossier

    @property
    def chapter(self) -> str:
        if not hasattr(self, "_chapter"):
            m = re.search(r"[A-Z]+", self.metadata.get("OVERHEIDop.dossiernummer", ""))
            self._chapter = m.group() if m else ""
        return self._chapter

    @property
    def number(self) -> str:
        """OVERHEIDop.ondernummer."""
        if not hasattr(self, "_number"):
            try:
                self._number = self.metadata["OVERHEIDop.ondernummer"]
            except KeyError:
                self._number = self.metadata["OVERHEIDop.publicationIssue"]
            except KeyError:
                self._number = self.metadata["DC.identifier"].split("-")[-1]
            except KeyError:
                self._number = ""
        return self._number

    @property
    def phase(self) -> str:
        if not hasattr(self, "_phase"):
            phase_re = re.compile(
                r"|".join(
                    [
                        "(?P<JV>jaarverslag)",
                        "(?P<OWB>vaststelling)",
                        "(?P<SUP1>voorjaarsnota)",
                        "(?P<SUP2>najaarsnota)",
                        "(?P<wet>wet van \d+ \w+)",
                    ]
                ),
                re.I,
            )
            m = phase_re.search(self.metadata.get("DC.title", ""))
            self._phase = m.lastgroup if m else ""
        return self._phase

    @property
    def source(self) -> str:
        if not hasattr(self, "_source"):
            self._source = self.metadata.get("source", "")
        return self._source

    @property
    def year(self) -> str:
        if not hasattr(self, "_year"):
            m = re.search("\d{4}", self.metadata.get("OVERHEIDop.dossiertitel", ""))
            if m:
                self._year = m.group()
            else:
                self._year = self.metadata.get("OVERHEIDop.jaargang", "")
        return self._year

    @property
    def text(self) -> str:
        return ""

    @property
    def id(self) -> Optional[str]:
        (id_key,) = [key for key in self.metadata if "identifier" in key] or None
        if id_key:
            return self.metadata[id_key]
        else:
            return "root"

    @property
    def index(self) -> Optional[str]:
        return self.id

    def to_dict(self, ids_only: bool = False) -> Dict[str, Any]:
        md = {
            "id": self.id,
            "token": self.token,
            "type": self.type,
            # "dossier": self.dossier,
            # "chapter": self.chapter,
            # "number": self.number,
            # "phase": self.phase,
            # "year": self.year,
            # "source": self.source,
            "config": self.config,
            "children": [child.to_dict() if ids_only else child.id for child in self],
        }
        md.update(self.metadata)
        return md

    def onderdelen(self, node: Onderdeel = None) -> Iterator[Union[Onderdeel, Kop]]:
        """Iterate over the document

        Returns an iterator that yields the elements of the parse tree, in document order (depth first).
        """
        if node is None:
            node = self
        yield node
        if hasattr(node, "children"):
            for c in node:  # type: ignore
                yield from self.onderdelen(c)

    def find(self, pred: Callable[[Onderdeel], bool]) -> List[Onderdeel]:
        """Search the root for Onderdelen for which the the callable `pred` returns `True`.

        Example::

          stuk.find(lambda onderdeel: onderdeel.type == "Tabel")

        """
        return [elt for elt in self.onderdelen() if pred(elt)]

    def find_string(self, string: str, case: bool = True) -> List[Onderdeel]:
        """Search the root for Onderdelen that contain `string`.

        Args:
            case: case sensitive

        Example::

          stuk.find_text("Minister")

        """
        if case:
            return self.find(lambda ond: string in ond.text)
        else:
            return self.find(lambda ond: string.lower() in ond.text.lower())

    def find_type(self, types: Union[str, List[str]]) -> List[Onderdeel]:
        """Search the root for Onderdelen with a certain type.

        if `types` is a list, return all onderdelen with type in types.

        Example::

            stuk.find_type("alinea")
            stuk.find_type(["tabel", "b-tabel"])

        """
        if isinstance(types, str):
            return self.find(lambda x: x.type == types)
        else:
            return self.find(lambda x: x.type in types)

    def find_token(self, token: Union[str, List[str]]) -> List[Onderdeel]:
        """Search the root for Onderdelen with a certain type.

        if `token` is a list, return all onderdelen with type in token.

        Example::

            stuk.find_type("alinea")
            stuk.find_type(["tabel", "b-tabel"])

        """
        if isinstance(token, str):
            return self.find(lambda x: x.token == token)
        else:
            return self.find(lambda x: x.token in token)

    def pprint(
        self,
        node: Onderdeel = None,
        prefix: str = "",
        skip_kids: bool = False,
        _repr: Callable[[Onderdeel], str] = repr,
    ) -> None:
        """Print an indented representation of the parse root.

        Args:
            prefix: string to keep track of indentation level
            skip_kids: if True, print (container) titles only
            _repr: callable that takes an Onderdeel and applies formatting
        """
        if node is None:
            node = self
        # NOTE: Dit kan niet:
        for n in node:
            if skip_kids:
                if hasattr(n, "children"):
                    print(prefix, _repr(n))
            else:
                print(prefix, _repr(n))
            prefix += "\t"
            if hasattr(n, "children"):
                for c in n:
                    self.pprint(c, prefix, skip_kids, _repr)
